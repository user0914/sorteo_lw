@props([
  'event' => null,
  'key' => null,
  'classes' => null,
  'title'   => '',
  'text'    => ''

])

  <button type="button" class="btn btn-sm btn-warning btn-primary {{ $classes }}" data-toggle="modal" data-target="#modal{{ $key }}">
    <i class='fas fa-star'></i>
  </button>
  <div class="modal fade" id="modal{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="modal{{ $key }}Label" aria-hidden="true" style="background-image: url({{ url('images/fondo1.png') }}) ; background-position: center; background-repeat: no-repeat;">
    <div class="modal-dialog modal-lg" role="document" style="opacity:0.9 !important; top: 7.5%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <pre>
            {{ $slot }}
          </pre>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
