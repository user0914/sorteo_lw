<x-admin.base title="Editar Paginas">
  <x-admin.back-btn route="admin.pages.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.pages.edit-pages :page='request()->id'>
    </div>
</div>
</x-admin>