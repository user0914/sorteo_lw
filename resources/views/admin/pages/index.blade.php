<x-admin.base title="Paginas">
  <x-admin.new-btn route="admin.pages.create" title="Agregar Pagina" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.pages.list-pages>
    </div>
</div>
</x-admin>