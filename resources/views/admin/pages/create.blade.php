<x-admin.base title="Agregar Pagina">
  <x-admin.back-btn route="admin.pages.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.pages.new-pages >
    </div>
</div>
</x-admin>