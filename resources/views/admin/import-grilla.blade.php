<x-admin.base title="Cargar aspirantes">
  <div class="row">
    <div class="col-md-12">
        <x-front.modal key="Categoria" classes="float-right" title="Agregar categoria">
          <livewire:admin.categoria.new-categoria>
      </x-front>

      <x-front.modal key="Sorteo" classes="float-right" title="Agregar sorteo">
        <livewire:admin.sorteo.new-sorteo>
      </x-front>
    </div>


    <div class="col-md-12">
      <livewire:admin.import-grilla>
    </div>
  </div>
</x-admin>