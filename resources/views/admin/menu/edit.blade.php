<x-admin.base title="Editar Menu">
  <x-admin.back-btn route="admin.menu.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.menu.edit-menu :menu='request()->id'>
    </div>
</div>
</x-admin>