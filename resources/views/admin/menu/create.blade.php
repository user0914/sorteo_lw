<x-admin.base title="Agregar Menu">
  <x-admin.back-btn route="admin.menu.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.menu.new-menu >
    </div>
</div>
</x-admin>