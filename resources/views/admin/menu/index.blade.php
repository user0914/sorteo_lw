<x-admin.base title="Menus">
  <x-admin.new-btn route="admin.menu.create" title="Agregar Menu" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.menu.list-menu/>
    </div>
</div>
</x-admin>