<x-admin.base title="Editar Usuario">
  <x-admin.back-btn route="admin.users.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.users.edit-users :user='request()->id'>
    </div>
</div>
</x-admin>