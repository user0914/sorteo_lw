<x-admin.base title="Agregar Carrusel">
  <x-admin.back-btn route="admin.slider.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.slider.new-slider>
    </div>
</div>
</x-admin>