<x-admin.base title="Carrusel">
  <x-admin.new-btn route="admin.slider.create" title="Agregar Carrusel" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.slider.list-slider>
    </div>
</div>
</x-admin>