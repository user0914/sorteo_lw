<x-admin.base title="Editar Carrusel">
  <x-admin.back-btn route="admin.slider.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.slider.edit-slider :slider='request()->id'>
    </div>
</div>
</x-admin>