<x-admin.base title="Editar Noticias">
  <x-admin.back-btn route="admin.articles.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.news.edit-article :article='request()->id'>
    </div>
</div>
</x-admin>