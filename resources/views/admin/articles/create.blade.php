<x-admin.base title="Agregar Noticias">
  <x-admin.back-btn route="admin.articles.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.news.new-article >
    </div>
</div>
</x-admin>