<x-admin.base title="Noticias">
  <x-admin.new-btn route="admin.articles.create" title="Agregar Noticia" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.news.list-articles>
    </div>
</div>
</x-admin>