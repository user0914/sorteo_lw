<x-admin.base title="Agregar Usuario">
  <x-admin.back-btn route="admin.users.index" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.users.new-users>
    </div>
</div>
</x-admin>