<x-admin.base title="Usuarios">
  <x-admin.new-btn route="admin.users.create" title="Agregar Usuario" />
  <div class="row">
    <div class="col-md-12">
      <livewire:admin.users.list-users>
    </div>
  </div>
</x-admin>