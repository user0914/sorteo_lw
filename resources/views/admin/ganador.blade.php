<x-admin.base title="">
<x-admin.card >
    <div class="row">    

      <div class="col-md-12">
        {{-- <div class="row">
          <x-admin.select 
              model="sorteo_selected" 
              title="Sorteo:" 
              :values="$sorteos" 
              classes="col-md-4" 
              tabindex=1
          />

          <x-admin.select 
              model="categoria_selected" 
              title="Categoria:" 
              :values="$categorias" 
              classes="col-md-4" 
              tabindex=2
          />


          <div class="col-md-4">
            <button type="button" class="float-right btn-sm btn-primary" wire:click="buscar">Primary</button>
          </div>
        </div> --}}

        <livewire:admin.aspirante.ganador :aspirante='request()->id'>
        {{-- <livewire:admin.aspirantes> --}}
      </div>
    </div>
</x-admin.card>
</x-admin>
