<x-admin.base title="Mi Perfil">
  <div class="row">
    <div class="col-md-9">
      <livewire:admin.profile.account>
    </div>
    <div class="col-md-3">
      <livewire:admin.profile.password>
    </div>
</div>
</x-admin>