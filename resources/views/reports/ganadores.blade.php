<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
  body { font-size:13px; font-family: DejaVu Sans, sans-serif; }
    table, td, th {
        border: 1px solid black;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

.right {
  float: right;
  width: 100px;
  padding: 10px;
}

.left {
  float: left;
  width: 100px;
  padding: 10px;
  padding-top: 14px;
}
</style>
<title> Listado de ganadores</title>
</head>
<body>

    <img src="images/membrete_completo.jpg" style="max-width: 200px;">
    <div style="margin-left: 520px;">Resistencia; {{ date('d/m/y', strtotime($fecha)) }}</div> 
<br>
@if($general)
    <div style="text-align: center; font-size:23px;"> Ganadores de todos los sorteos </div><br>
@else
    <div style="text-align: center; font-size:23px;"> Ganadores {{$ganadores[0]->sorteo->nombre}} </div><br>
@endif
        <table class="table table-striped table-bordered table-sm">
            <thead>
                <tr style="font-size: 14px;">
                    <th>Num. Orden</th>
                    <th>Titular</th>
                    <th>Doc.</th>
                    @if(!$general) <th>Categoria</th>  @endif
                </tr>
            </thead>
            <tbody>
                @forelse ($ganadores as $ganador)
                <tr style="font-size: 14px;">
                    <td>
                        {{ $ganador->num_orden }}
                    </td>
                    <td>
                        {{ $ganador->apellidos . ', ' . $ganador->nombres }}
                    </td>
                    <td>
                        {{ $ganador->num_doc }}
                    </td>
                    @if(!$general) <td>{{$ganador->categoria->nombre}}</td>  @endif
                </tr>
                @empty
                <tr>
                    <td colspan="4">
                        <div class="callout callout-info">
                        <p>Todavia no hay ganadores</p>
                        </div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table> 

</body>
</html>

