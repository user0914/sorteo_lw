<div class="row">
    <div class="col-md-9">
        <x-admin.card title="Datos del Menu">
            @include('livewire.admin.menu.form')
        </x-admin.card>
    </div>
    <div class="col-md-3">
        <x-admin.card title="Opciones">
            <x-admin.save-btn event="save_menu" updatedtext="{{ $menu->updated_at->format('d/m/y H:i:s') }}" newLine=true />
            <hr>
             <p> Estado: 
                <span class="badge badge-{{ $menu->published ? 'success':'danger' }}">
                    {{ $menu->published ? 'Publicada':'No Publicada' }}
                </span>
            </p>
            <div 
                wire:click='toggle_status' 
                class="btn btn-{{ $menu->published ? 'danger':'success' }}"
            >
                {{ $menu->published ? 'Despublicar':'Publicar' }}
            </div>
            <hr>
            <x-admin.delete-btn 
                :key="$menu->id" 
                event="delete_menu"  
                text="Desea eliminar el menu: {{ $menu->title }}"
            />
        </x-admin.card>
    </div>
</div>
