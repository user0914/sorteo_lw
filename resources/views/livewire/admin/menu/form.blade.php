<div class="row">
    <x-admin.input 
        title="Título" 
        model="menu.title" 
        required=true 
        tabindex=1 
        classes="col-md-4" 
        placeholder="Inicio"
    />
    <x-admin.input 
        title="Link (Opcional)" 
        model="menu.link" 
        tabindex=2 
        classes="col-md-8" 
        placeholder="http://google.com"
        :disabled="$menu->pages()->exists()"
        hint="{{ $menu->pages()->exists() ? 'Descativado porque este menu tiene paginas' : false }}"
    />
</div>
<div class="row">
    <x-admin.input 
        title="Posición" 
        model="menu.position" 
        tabindex=3 
        classes="col-md-2" 
        placeholder="10"
    />
    <x-admin.input 
        title="Icono" 
        model="menu.icon" 
        tabindex=4 
        classes="col-md-6"
        placeholder="address-book"
        :disabled="$menu->pages()->exists() || $menu->link"
        hint="{{ $menu->pages()->exists() ? 'Descativado porque este menu tiene paginas' : 'Lista de iconos: https://fontawesome.com/icons?d=gallery&m=free, poner solo el nombre' }}"
    />
    <x-admin.select 
        model="menu.target" 
        title="Abrir en:" 
        :values="$targets" 
        classes="col-md-4" 
        tabindex=5
        :disabled="$menu->pages()->exists()"
        hint="{{ $menu->pages()->exists() ? 'Descativado porque este menu tiene paginas' : false }}"
    />
</div>
<div class="row">
    <x-admin.checkbox 
        title="En el Inicio?" 
        model="menu.to_home" 
        tabindex=5 
        classes="col-md-4"
        :disabled="$menu->pages()->exists() || $menu->link"
        hint="{{ $menu->pages()->exists() ? 'Descativado porque este menu tiene paginas' : false }}"
    />
</div>