<x-admin.card title="Listado de Menus">
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Posición</th>
                <th>Publicado</th>
                <th>Paginas</th>
                <th>En Inicio?</th>
                <th>Link</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($menus as $menu)
            <tr>
                <td>
                    <a href="{{ route('admin.menu.edit', $menu->id) }}">{{ $menu->title }}</a>
                </td>
                <td>{{ $menu->position }}</td>
                <td>
                    <span class="badge bg-{{ $menu->published ? 'success' : 'danger' }}">
                        {{ $menu->published ? 'Si' : 'No' }}
                    </span>
                </td>
                <td>{{ $menu->pages_count }}</td>
                <td>
                    <span class="badge bg-{{ $menu->to_home ? 'success' : 'danger' }}">
                        {{ $menu->to_home ? 'Si' : 'No' }}
                    </span>
                </td>
                <td>
                    @if($menu->link)
                    {{ $menu->link }}
                    @else
                    <span class="badge bg-secondary"> NO </span>
                    @endif
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="6">
                    <div class="callout callout-info">
                    <p>Todavia No hay Menu</p>
                    </div>
                </td>
            </tr>
            @endforelse
        </tbody>
    </table> 
    <p>
        {{ $menus->links() }}
    </p>
</x-admin.card>
