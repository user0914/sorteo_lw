<x-admin.card title="Importador de archivo excel (.xls)">
    <div class="row">
        <x-admin.select 
            model="sorteo_selected" 
            title="Sorteo:" 
            :values="$sorteos" 
            tabindex=1
        />
    </div>

    <div class="row">
        <x-admin.select 
            model="categoria_selected" 
            title="Categoria:" 
            :values="$categorias" 
            tabindex=2
        />
    </div>

    <div class="row">
        <x-front.file id="upload{{ $iteration }}" title="Seleccione un archivo" model="file" tabindex=3 classes="col-md-2">
        </x-front.file>
    </div>
    <br>
    <x-admin.save-btn event="save_grilla" />
</x-admin.card>