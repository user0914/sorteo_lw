<div class="row">
    <div class="col-md-9">
        <x-admin.card title="Datos del Carrusel">
            @include('livewire.admin.slider.form')
        </x-admin.card>
    </div>
    <div class="col-md-3">
        <x-admin.card title="Opciones">
            <x-admin.save-btn event="save_slider" updatedtext="{{ $slider->updated_at->format('d/m/y H:i:s') }}" newLine=true />
            <hr>
             <p> Estado: 
                <span class="badge badge-{{ $slider->published ? 'success':'danger' }}">
                    {{ $slider->published ? 'Publicada':'No Publicada' }}
                </span>
            </p>
            <div 
                wire:click='toggle_status' 
                class="btn btn-{{ $slider->published ? 'danger':'success' }}"
            >
                {{ $slider->published ? 'Despublicar':'Publicar' }}
            </div>
            <hr>
            <x-admin.delete-btn 
                :key="$slider->id" 
                event="delete_slider"  
                text="Desea eliminar la diapositiva: {{ $slider->title }}"
            />
        </x-admin.card>
        @if ($slider->image)
            <x-admin.card title="Foto">
                <img src="{{ asset($slider->image) }}" class="img-fluid rounded" />
                <div class="btn btn-danger mt-2" wire:click='delete_photo'>
                    <i class="fas fa-trash-alt"></i> Borrar Foto
                </div>
            </x-admin.card>
        @endif
    </div>
</div>
