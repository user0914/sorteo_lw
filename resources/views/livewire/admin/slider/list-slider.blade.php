<x-admin.card title="Listado de Carrusel">
    <table class="table table-striped table-bordered table-sm">
        <thead>
            <tr>
                <th></th>
                <th class="col">Titulo</th>
                <th>Fecha</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($sliders as $slider)
            <tr>
                <td>
                    @if ($slider->image != null)
                        <img src="{{ asset($slider->image) }}" class="mini_img">
                    @endif
                </td>
                <td>
                    <a href="{{ route('admin.slider.edit', $slider->id) }}">{{ $slider->title }}</a>
                </td>
                <td>{{ $slider->created_at->format('d/m/y') }}</td>
                <td>
                    <span class="badge bg-{{ $slider->published ? 'success' : 'danger' }}">
                        {{ $slider->published ? 'Publicada' : 'No Publicada' }}
                    </span>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4">
                    <div class="callout callout-info">
                    <p>Todavia No hay Carrusel</p>
                    </div>
                </td>
            </tr>
            @endforelse
        </tbody>
    </table> 
    <p>
        {{ $sliders->links() }}
    </p>
</x-admin.card>
