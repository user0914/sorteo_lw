<div class="row">
    <x-admin.input title="Título" model="slider.title" required=true tabindex=1 classes="col-md-12"/>
</div>
<div class="row">
    <x-admin.file  title="Foto" model="image" tabindex=4 classes="col-md-2" :photo="$image">
        <i class="fas fa-image"></i>
        Buscar Foto
    </x-admin.file>
</div>