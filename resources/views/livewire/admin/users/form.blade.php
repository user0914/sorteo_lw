<div class="row">
    <x-admin.input title="Nombre" model="user.name" required=true tabindex=1 classes="col-md-4"/>
    <x-admin.input title="Apellido" model="user.last_name" required=true tabindex=2 classes="col-md-4"/>
    <x-admin.input title="email" type="email" model="user.email" :disabled="$edit" tabindex=3 classes="col-md-4"/>
</div>

@if (!$edit)
<div class="row">
    <x-admin.input title="Contraseña" type="password" model="password" required=true tabindex=4 classes="col-md-4"/>
    <x-admin.input title="Confirmar Contraseña" type="password" model="password_confirmation" required=true tabindex=5 classes="col-md-4"/>
</div>
@endif