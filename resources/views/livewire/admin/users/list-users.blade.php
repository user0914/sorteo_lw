<x-admin.card title="Listado de Usuarios">
    <table class="table table-striped table-bordered table-sm">
        <thead>
            <tr>
                <th>Apellido y Nombre</th>
                <th>Email</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($users as $user)
            <tr>
                <td>
                    <a href="{{ route('admin.users.edit', $user->id) }}">{{ $user->last_name }}, {{ $user->name }}</a>
                </td>
                <td>{{ $user->email }}</td>
                <td>
                    <span class="badge bg-{{ $user->active ? 'success' : 'danger' }}">
                        {{ $user->active ? 'Activo' : 'Suspendido' }}
                    </span>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4">
                    <div class="callout callout-info">
                    <p>Todavia No hay Usuarios</p>
                    </div>
                </td>
            </tr>
            @endforelse
        </tbody>
    </table> 
    <p>
        {{ $users->links() }}
    </p>
</x-admin.card>
