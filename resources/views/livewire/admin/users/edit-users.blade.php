<div class="row">
    <div class="col-md-9">
        <x-admin.card title="Datos del Usuario">
            @include('livewire.admin.users.form')
        </x-admin.card>
    </div>
    <div class="col-md-3">
        <x-admin.card title="Opciones">
            <x-admin.save-btn event="save_user" updatedtext="{{ $user->updated_at->format('d/m/y H:i:s') }}" newLine=true />
            <hr>
             <p> Estado: 
                <span class="badge badge-{{ $user->active ? 'success':'danger' }}">
                    {{ $user->active ? 'Activo':'Suspendido' }}
                </span>
            </p>
            <div 
                wire:click='toggle_status' 
                class="btn btn-{{ $user->active ? 'danger':'success' }}"
            >
                {{ $user->active ? 'Suspender':'Activar' }}
            </div>
            @if ($user->id != auth()->id()) 
            <hr>
            <x-admin.delete-btn 
                :key="$user->id" 
                event="delete_user"  
                text="Desea eliminar el usuario: {{ $user->full_name }}"
            />
            @endif
        </x-admin.card>
    </div>
</div>
