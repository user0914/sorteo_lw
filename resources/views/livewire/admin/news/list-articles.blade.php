<x-admin.card title="Listado de Noticias">
    <table class="table table-striped table-bordered table-sm">
        <thead>
            <tr>
                <th></th>
                <th class="col">Titulo</th>
                <th>Fecha</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($articles as $article)
            <tr>
                <td>
                    @if ($article->photo != null)
                        <img src="{{ asset($article->photo) }}" class="mini_img">
                    @endif
                </td>
                <td>
                    <a href="{{ route('admin.articles.edit', $article->id) }}">{{ $article->title }}</a>
                </td>
                <td>{{ $article->created_at->format('d/m/y') }}</td>
                <td>
                    <span class="badge bg-{{ $article->published ? 'success' : 'danger' }}">
                        {{ $article->published ? 'Publicada' : 'No Publicada' }}
                    </span>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4">
                    <div class="callout callout-info">
                    <p>Todavia No hay Noticias</p>
                    </div>
                </td>
            </tr>
            @endforelse
        </tbody>
    </table> 
    <p>
        {{ $articles->links() }}
    </p>
</x-admin.card>
