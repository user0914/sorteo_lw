<div class="row">
    <div class="col-md-9">
        <x-admin.card title="Datos de la Noticia">
            @include('livewire.admin.news.form')
        </x-admin.card>
    </div>
    <div class="col-md-3">
        <x-admin.card title="Opciones">
            <x-admin.save-btn event="save_article" />
        </x-admin.card>
    </div>
</div>
