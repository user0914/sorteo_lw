<div class="row">
    <div class="col-md-9">
        <x-admin.card title="Datos de la Noticia">
            @include('livewire.admin.news.form')
        </x-admin.card>
    </div>
    <div class="col-md-3">
        <x-admin.card title="Opciones">
            <x-admin.save-btn event="save_article" updatedtext="{{ $article->updated_at->format('d/m/y H:i:s') }}" newLine=true />
            <hr>
             <p> Estado: 
                <span class="badge badge-{{ $article->published ? 'success':'danger' }}">
                    {{ $article->published ? 'Publicada':'No Publicada' }}
                </span>
            </p>
            <div 
                wire:click='toggle_status' 
                class="btn btn-{{ $article->published ? 'danger':'success' }}"
            >
                {{ $article->published ? 'Despublicar':'Publicar' }}
            </div>
            <hr>
            <x-admin.delete-btn 
                :key="$article->id" 
                event="delete_article"  
                text="Desea eliminar la noticia: {{ $article->title }}"
            />
        </x-admin.card>
        @if ($article->photo)
            <x-admin.card title="Foto">
                <img src="{{ asset($article->photo) }}" class="img-fluid rounded" />
                <div class="btn btn-danger mt-2" wire:click='delete_photo'>
                    <i class="fas fa-trash-alt"></i> Borrar Foto
                </div>
            </x-admin.card>
        @endif
    </div>
</div>
