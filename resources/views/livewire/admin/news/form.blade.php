<div class="row">
    <x-admin.input title="Título" model="article.title" required=true tabindex=1 classes="col-md-12"/>
</div>
<div class="row">
    <x-admin.textarea title="Resumen" model="article.resumen" required=true tabindex=2 classes="col-md-12"/>
</div>

<div class="row">
    <x-admin.ckeditor title="Contenido" model="article.content" required=true tabindex=3 classes="col-md-12" rows=14
        ckeditorPath="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"
    />
</div>
<div class="row">
    <x-admin.file  title="Foto" model="photo" tabindex=4 classes="col-md-2" :photo="$photo">
        <i class="fas fa-image"></i>
        Nueva Foto
    </x-admin.file>
</div>