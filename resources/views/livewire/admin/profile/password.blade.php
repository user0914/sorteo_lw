<div>
    <x-admin.card title="Actualizar Contraseña">
        <div class="row">
            <x-admin.input title="Vieja Contraseña" type="password" model="old_password" required=true tabindex=4 classes="col-md-12"/>
        </div>
        <div class="row">
            <x-admin.input title="Nueva Contraseña" type="password" model="new_password" required=true tabindex=5 classes="col-md-12"/>
        </div>
        <div class="row">
            <x-admin.input title="Confirmar Nueva Contraseña" type="password" model="new_password_confirmation" required=true tabindex=6 classes="col-md-12"/>
        <div class="row">
            <div class="col-md-12">
                <hr>
                <x-admin.save-btn title="Actualizar" event="update_password" updatedtext="{{ $user->updated_at->format('d/m/y H:i:s') }}" newLine=true />
            </div>
        </div>
    </x-admin.card>
</div>
