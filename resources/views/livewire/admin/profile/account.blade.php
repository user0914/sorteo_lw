<div>
    <x-admin.card title="Mis Datos">
        <div class="row">
            <x-admin.input title="Nombre" model="user.name" required=true tabindex=1 classes="col-md-4"/>
            <x-admin.input title="Apellido" model="user.last_name" required=true tabindex=2 classes="col-md-4"/>
            <x-admin.input title="email" type="email" model="user.email" disabled=true tabindex=3 classes="col-md-4"/>
        </div>
        <div class="row">
            <div class="col-md-12">
                <hr>
                <x-admin.save-btn event="save_profile" updatedtext="{{ $user->updated_at->format('d/m/y H:i:s') }}" />
            </div>
        </div>
    </x-admin.card>
</div>
