<div>
    <div class="row">
        <x-admin.info-box 
            classes="col-12 col-md-3"
            bg="success"
            icon="fas fa-newspaper"
            title="Noticias"
            :value="$articles"
            route="admin.articles.index"
        />

        <x-admin.info-box 
            classes="col-12 col-md-3"
            bg="danger"
            icon="fas fa-file-alt"
            title="Paginas"
            :value="$pages"
            route="admin.pages.index"
        />

        <x-admin.info-box 
            classes="col-12 col-md-3"
            icon="fas fa-images"
            title="Carrusel"
            :value="$sliders"
            route="admin.slider.index"

        />

        <x-admin.info-box 
            classes="col-12 col-md-3"
            bg="warning"
            icon="fas fa-users"
            title="Usuarios"
            :value="$users"
            route="admin.users.index"
        />
    </div>
</div>
