@if ($page->id)
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-info">
            <h4>Url de la pagina</h4>
            <p>
                {{ route('pages.show', $page->id) }} - 
                <a href="{{ route('pages.show', $page->id) }}" target="_blank">visitar</a>
            </p>
        </div>
    </div>
</div>
@endif

<div class="row">
    <x-admin.input title="Título" model="page.title" required=true tabindex=1 classes="col-md-12"/>
</div>
<div class="row">
    <x-admin.ckeditor title="Contenido" model="page.content" required=true tabindex=3 classes="col-md-12"
        ckeditorPath="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"
    />
</div>
<div class="row">
    <x-admin.file  title="Foto" model="image" tabindex=4 classes="col-md-2" :photo="$image">
        <i class="fas fa-image"></i>
        Buscar Foto
    </x-admin.file>
    <x-admin.select model="page.menu_id" title="Menú" :values="$menus" placeholder="No menu" classes="col-md-4" tabindex=5 no_opcion="No Menú"/>
</div>