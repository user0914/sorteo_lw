<div class="row">
    <div class="col-md-9">
        <x-admin.card title="Datos de la Noticia">
            @include('livewire.admin.pages.form')
        </x-admin.card>
    </div>
    <div class="col-md-3">
        <x-admin.card title="Opciones">
            <x-admin.save-btn event="save_page" updatedtext="{{ $page->updated_at->format('d/m/y H:i:s') }}" newLine=true />
            <hr>
             <p> Estado: 
                <span class="badge badge-{{ $page->published ? 'success':'danger' }}">
                    {{ $page->published ? 'Publicada':'No Publicada' }}
                </span>
            </p>
            <div 
                wire:click='toggle_status' 
                class="btn btn-{{ $page->published ? 'danger':'success' }}"
            >
                {{ $page->published ? 'Despublicar':'Publicar' }}
            </div>
            <hr>
            <x-admin.delete-btn 
                :key="$page->id" 
                event="delete_page"  
                text="Desea eliminar la siguiente pagina: {{ $page->title }}"
            />
        </x-admin.card>
        @if ($page->image)
            <x-admin.card title="Foto">
                <img src="{{ asset($page->image) }}" class="img-fluid rounded" />
                <div class="btn btn-danger mt-2" wire:click='delete_photo'>
                    <i class="fas fa-trash-alt"></i> Borrar Foto
                </div>
            </x-admin.card>
        @endif
    </div>
</div>
