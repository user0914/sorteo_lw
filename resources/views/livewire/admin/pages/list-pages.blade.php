<x-admin.card title="Listado de Paginas">
    <table class="table table-striped table-bordered table-sm">
        <thead>
            <tr>
                <th></th>
                <th class="col">Titulo</th>
                <th>Fecha</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($pages as $page)
            <tr>
                <td>
                    @if ($page->image != null)
                        <img src="{{ asset($page->image) }}" class="mini_img">
                    @endif
                </td>
                <td>
                    <a href="{{ route('admin.pages.edit', $page->id) }}">{{ $page->title }}</a>
                </td>
                <td>{{ $page->created_at->format('d/m/y') }}</td>
                <td>
                    <span class="badge bg-{{ $page->published ? 'success' : 'danger' }}">
                        {{ $page->published ? 'Publicada' : 'No Publicada' }}
                    </span>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4">
                    <div class="callout callout-info">
                    <p>Todavia No hay Paginas</p>
                    </div>
                </td>
            </tr>
            @endforelse
        </tbody>
    </table> 
    <p>
        {{ $pages->links() }}
    </p>
</x-admin.card>
