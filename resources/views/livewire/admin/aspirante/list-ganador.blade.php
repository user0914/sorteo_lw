<div>

<div class="row">
        <x-admin.select 
            model="sorteo_selected" 
            title="Sorteo:" 
            :values="$sorteos" 
            classes="col-md-4" 
            tabindex=1
        />

        <x-admin.select 
            model="categoria_selected" 
            title="Categoria:" 
            :values="$categorias" 
            classes="col-md-6" 
            tabindex=2
        />
        <div class="form-group">
            <button style="margin-top:32px;" class="btn btn-primary mx-auto" wire:click="filrar"> Buscar</button>
        </div>
</div>
<hr><br>

<div class="row">

<div style="text-align: center; font-size:23px;"> Ganadores </div><br>
        <table class="table table-striped table-bordered table-sm">
            <thead>
                <tr>
                    <th>Num. Orden</th>
                    <th>Titular</th>
                    <th>Num. Doc.</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($aspirantes as $aspirante)
                <tr>
                    <td>
                        {{ $aspirante->num_orden }}
                    </td>
                    <td>
                        {{ $aspirante->apellidos . ', ' . $aspirante->nombres }}
                    </td>
                    <td>
                        {{ $aspirante->num_doc }}
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">
                        <div class="callout callout-info">
                        <p>Todavia no hay ganadores</p>
                        </div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table> 
        <p>
            {{ $aspirantes->links() }}
        </p>
        </div>
</div>