<div>
    <div>
    <p style="font-size:55px; margin-top:0; color:#04c1f3; text-align: center;">{{ $aspirante->sorteo->nombre }}</p> 
</div>
<div>    
<p style="font-size:35px; text-align: center;"><b>Orden: </b>{{ $aspirante->num_orden }}</p> 
<div>    
<p style="font-size:35px; text-align: left;"><b>Nombre: </b>{{ $aspirante->nombres . ' ' . $aspirante->apellidos }}</p> 
<p style="font-size:35px; text-align: left;"><b>Documento: </b>{{ $aspirante->num_doc }}</p> 
</div>

<button class="btn btn-warning btn-sm mx-auto float-right" wire:click="ganador"> Ganador! </button>
</div>