<div>
<div class="row">
        <x-admin.select 
            model="sorteo_selected" 
            title="Sorteo:" 
            :values="$sorteos" 
            classes="col-md-4" 
            noOpcion="Escoga un sorteo"
            tabindex=1
        />

        <x-admin.select 
            model="categoria_selected" 
            title="Categoria:" 
            :values="$categorias" 
            classes="col-md-4" 
            noOpcion="Escoga una categoria"
            tabindex=2
        />
          <x-admin.input title="Num. Orden" model="num_orden" required=true tabindex=3 classes="col-md-3"/>
        <div class="form-group">
            <button style="margin-top:32px;" class="btn btn-primary mx-auto"> Buscar</button>
        </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">
    <div style="text-align: center; font-size:23px;"> Ganadores </div><br>
        <table class="table table-striped table-bordered table-sm">
            <thead>
                <tr style="font-size: 14px;">
                    <th>Num. Orden</th>
                    <th>Titular</th>
                    <th>Doc.</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($ganadores as $ganador)
                <tr style="font-size: 14px;">
                    <td>
                        {{ $ganador->num_orden }}
                    </td>
                    <td>
                        {{ $ganador->apellidos . ', ' . $ganador->nombres }}
                    </td>
                    <td>
                        {{ $ganador->num_doc }}
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">
                        <div class="callout callout-info">
                        <p>Todavia no hay ganadores</p>
                        </div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table> 
        <p>
            {{ $ganadores->links() }}
            Total ganadores de esta categoria {{ $ganadores->total() }}
        </p>
        <button type="button" style="margin-left: 5px;" class="btn-sm float-right btn-primary" wire:click="reporte(0)"> 
            Imprimir general
             <i class='fas fa-print'></i>
        </button>
        {{-- <button type="button"  class="btn-sm float-right btn-primary" wire:click="reporte(1)"> 
            Imprimir general
             <i class='fas fa-print'></i>
        </button> --}}
    </div>

    <div class="col-md-6">
    <div style="text-align: center; font-size:23px;"> Aspirantes </div><br>
        <table class="table table-striped table-bordered table-sm">
            <thead>
                <tr style="font-size: 14px;">
                    {{-- <th>Sorteo</th>
                    <th>Categoria</th> --}}
                    <th>Num. Orden</th>
                    <th>Titular</th>
                    <th>Doc.</th>
                    <th>Ganador</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($aspirantes as $aspirante)
                <tr>
                    {{-- <td>
                        {{ $aspirante->sorteo->nombre}}
                    </td>
                    <td>
                        {{ $aspirante->categoria->nombre }}
                    </td> --}}
                    <td>
                        {{ $aspirante->num_orden }}
                    </td>
                    <td>
                        {{ $aspirante->apellidos . ', ' . $aspirante->nombres }}
                    </td>
                    <td>
                        {{ $aspirante->num_doc }}
                    </td>
                    <td>
                    {{-- <a class="btn btn-sm display-4 btn-warning" href="{{ route('admin.ganador', $aspirante->id) }}">        <i class='fas fa-star'></i></a> --}}
                    {{-- <a class="btn btn-sm display-4 btn-warning" href=" wire:click.prevent="edit({{$aspirante->id}})">        <i class='fas fa-star'></i></a> --}}
                        <a href=""  class="btn btn-warning" wire:click.prevent="edit({{ $aspirante->id }})"> 
                        <i class='fas fa-star'></i>
                        </a>
                    </td>
                </tr>
                @empty
                <tr style="font-size: 14px;">
                    <td colspan="4">
                        <div class="callout callout-info">
                        <p>Todavia No hay Aspirantes</p>
                        </div>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table> 
        <p>
            {{ $aspirantes->links() }}
        </p>
    </div>

@if($aspiranteGanador)
    <div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="  margin: 0; margin-right: auto; margin-left: auto; width: 100%; background-image: url({{ url('images/fondo1.png') }}) ; background-position: center; background-repeat: no-repeat;">
    <div class="modal-dialog modal-lg" role="document" style="opacity:0.9 !important; top: 12%;">
      <div class="modal-content">
        <div class="modal-body">
            <div>
                <div>
                    <p style="font-size:45px; margin-top:0; color:#04c1f3; text-align: center;"> Sorteo {{ $aspiranteGanador->sorteo->nombre }}</p> 
                </div>
            <div>    
                @if(!$aspiranteGanadorExistente)
                <p style="font-size:35px;"><b>Orden: </b>{{ $aspiranteGanador->num_orden }}</p> 
                <div>    
                <p style="font-size:35px; text-align: left;"><b>Nombre: </b>{{ $aspiranteGanador->nombres . ' ' . $aspiranteGanador->apellidos }}</p> 
                <p style="font-size:35px; text-align: left;"><b>Documento: </b>{{ $aspiranteGanador->num_doc }}</p> 
                </div>
                <br>
                @else
                 <p style="font-size:30px;"><b> </b>{{ $aspiranteGanador->nombres }} ya salió ganador en la categoría <u> {{ $aspiranteGanadorExistente->categoria->nombre }} </u>                 
                @endif

            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            @if(!$aspiranteGanadorExistente)
              <button type="button" class="btn btn-warning" wire:click.prevent="ganador({{ $aspiranteGanador->id }})"> Ganador! </button>
            @endif
        </div>
      </div>
    </div>
  </div>
@endif
</div>

@section('js')
    <script>
        Livewire.on('show-form', result => {
            $('#modal').modal('show');
        })
        Livewire.on('submit-form', result => {
            $("#modal").modal('hide');
        })
    </script>
    @stop
</div>