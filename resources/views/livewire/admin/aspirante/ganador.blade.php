@section('css')
<style>

</style>
@stop

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="  margin: 0; margin-right: auto; margin-left: auto; width: 100%; background-image: url({{ url('images/fondo1.png') }}) ; background-position: center; background-repeat: no-repeat;">
    <div class="modal-dialog modal-lg" role="document" style="opacity:0.9 !important; top: 12%;">
      <div class="modal-content">
        <div class="modal-body">
            <div>
                <div>
                    <p style="font-size:55px; margin-top:0; color:#04c1f3; text-align: center;">{{ $aspiranteGanador->sorteo->nombre }}</p> 
                </div>
            <div>    
                <p style="font-size:35px;"><b>Orden: </b>{{ $aspiranteGanador->num_orden }}</p> 
                <div>    
                <p style="font-size:35px; text-align: left;"><b>Nombre: </b>{{ $aspiranteGanador->nombres . ' ' . $aspiranteGanador->apellidos }}</p> 
                <p style="font-size:35px; text-align: left;"><b>Documento: </b>{{ $aspiranteGanador->num_doc }}</p> 
                </div>
                <br>

            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-warning" wire:click="ganador"> Ganador! </button>

        </div>
      </div>
    </div>
  </div>

  @section('js')
    <script type="text/javascript">
        $(window).on('load', function() {
            $('#modal').modal('show');
        });
    </script>

    @stop
</div>
