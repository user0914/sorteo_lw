<div>

    <div class="row">
        <div class="col-md-12">
            <div style="text-align: center; font-size:23px;"> Ganadores </div><br>
            <table class="table table-striped table-bordered table-sm">
                <thead>
                    <tr style="font-size: 14px;">
                        <th>Sorteo</th>
                        <th>Categoria</th>
                        <th>Cant aspirantes</th>
                        <th>Borrar</th>
                        <th>Desactivar</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($sorteos as $sorteo)
                        <tr style="font-size: 14px;">
                            <td>
                                {{ $sorteo->sorteo }}
                            </td>
                            <td>
                                {{ $sorteo->categoria }}
                            </td>
                            <td>
                                {{ $sorteo->aspirantes }}
                            </td>
                            <td>
                                <x-admin.delete-btn 
                                    :key="$sorteo->sorteo_id" 
                                    event="soft_delete( {{$sorteo->sorteo_id }})"  
                                    text="Desea borrar permanentemente el sorteo: {{ $sorteo->sorteo }}"
                                />
                            </td>
                            <td>
                                <x-admin.delete-btn
                                    :key="$sorteo->sorteo_id" 
                                    event="delete( {{$sorteo->sorteo_id }})"  
                                    text="Desea desactivar el sorteo : {{ $sorteo->sorteo }}"
                                />
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                <div class="callout callout-info">
                                    <p>Todavia no hay sorteos cargados</p>
                                </div>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>


        </div>

    </div>
</div>
