<div class="container">
        @include('livewire.admin.categoria.form')
    <button class="btn btn-primary btn-sm mx-auto float-right" wire:click="save_categoria"> Guardar</button>


    @section('js')
    <script>
        Livewire.on('newDocumentationModal', result => {
            $('#modal' + result).modal('toggle');
            $(".modal-backdrop").remove();
        })
    </script>
    @stop
</div>