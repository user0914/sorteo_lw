<?php

use App\Models\Aspirante;
use App\Http\Livewire\Front\Articles;
use App\Http\Livewire\Front\PageShow;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Front\ArticlesShow;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','/admin/sorteo');
// Route::get('/', Home::class)->name('home');
Route::get('/noticias', Articles::class)->name('articles');
Route::get('/noticias/{article}', ArticlesShow::class)->name('articles.show');
Route::get('/pagina/{page}', PageShow::class)->name('pages.show');



Auth::routes();

