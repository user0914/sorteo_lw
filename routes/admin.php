<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Admin\Sorteo\IndexSorteo;
use App\Http\Livewire\Admin\Aspirante\ListAspirante;
use App\Http\Livewire\Admin\SorteosGanadores;

Route::redirect('/','/admin/sorteo');
Route::view('dashboard', 'admin.dashboard')->name('admin.dashboard');
Route::view('profile', "admin.profile.account")->name('admin.profile.account');
Route::view('grilla', 'admin.import-grilla')->name('admin.grilla');

Route::view('sorteo', 'admin.sorteo')->name('admin.sorteo');

Route::view('sorteos', 'admin.sorteo.list-sorteo')->name('admin.sorteos');

Route::view('ganador/{id}', 'admin.ganador')->name('admin.ganador');

$routes = ['articles', 'pages', 'menu', 'slider', 'users'];

foreach ($routes as $route) {
  Route::view($route, 'admin.'.$route.'.index')->name('admin.'.$route.'.index');
  Route::view($route.'/create', 'admin.'.$route.'.create')->name('admin.'.$route.'.create');
  Route::view($route.'/edit/{id}', 'admin.'.$route.'.edit')->name('admin.'.$route.'.edit');
}
