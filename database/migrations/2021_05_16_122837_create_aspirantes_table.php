<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAspirantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspirantes', function (Blueprint $table) {
            $table->increments('id');

            //VIEJO
            // $table->string('num_orden')->nullable()->default(null);
            // $table->string('num_inscripcion')->nullable()->default(null);
            // $table->string('titular')->nullable()->default(null);

            // $table->string('apellido')->nullable()->default(null);
            // $table->string('primer_nombre')->nullable()->default(null);
            // $table->string('segundo_nombre')->nullable()->default(null);
            // END VIEJO

            $table->string('num_orden')->nullable()->default(null);
            $table->string('num_inscripcion')->nullable()->default(null);
            $table->string('nombres')->nullable()->default(null);
            $table->string('apellidos')->nullable()->default(null);

            $table->string('num_doc')->nullable()->default(null);
            $table->integer('categoria_id')->nullable()->default(null);
            $table->integer('sorteo_id')->nullable()->default(null);
//            $table->boolean('ganador')->default('0');
            $table->boolean('ganador')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspirantes');
    }
}
