<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Samuel',
            'last_name' => 'Obando',
            'email'     => 'samuelo@ipduv.com',
            'password'  => Hash::make('123456'),
            'active'    => true
        ]);

        User::create([
            'name'      => 'Admin',
            'last_name' => 'Admin',
            'email'     => 'admin@admin.com',
            'password'  => Hash::make('admin@admin.com'),
            'active'    => true
        ]);
    }
}
