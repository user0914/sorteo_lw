<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entidad = new Categoria();
		$entidad->nombre = '1 o 2 hijos';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = '3 o más hijos';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'Antiguedad';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'Discapacidad';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'General';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'Sin hijos';
        $entidad->save();
    }
}
