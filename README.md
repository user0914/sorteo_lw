# Proyecto Sorteo IPDUV
Descripción


## Entornos


[Pruebas](http://url-prueba.chaco.gob.ar/)
[Producción](https://url.chaco.gob.ar/)




## Instalación


Clonar el repositorio y luego


```console
cd sorteo_lw
docker-compose up -d
docker exec sorteo_lw-php composer install
```

## Agregar al .env

```console
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=sorteo
DB_USERNAME=root
DB_PASSWORD=admin
```


#### Variables adicionales .env.local:


#### Database


```console
docker exec sorteo_lw-php php artisan migrate
```


#### Docker url
   * http://127.0.0.1:8082


### Usado
## Nombrar todos los frameworks utilizados.
- Laravel Versión 8
