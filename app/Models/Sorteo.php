<?php

namespace App\Models;

use App\Models\Aspirante;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Sorteo extends Model
{
    use HasFactory;
    use SoftDeletes;
 
    protected $dates = ['deleted_at'];

    public function aspirantes()
    {
        return $this->hasMany(Aspirante::class);
    }
}
