<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aspirante extends Model
{
    use HasFactory;
    
    protected $fillable = ['num_orden', 'nombres', 'apellidos', 'primer_nombre', 'segundo_nombre', 'apellido', 'num_inscripcion', 'titular', 'num_doc',
    'categoria_id', 'sorteo_id', 'ganador', 'created_at', 'updated_at'];

    public function sorteo()
    {
        return $this->belongsTo(Sorteo::class, 'sorteo_id');
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }
}
