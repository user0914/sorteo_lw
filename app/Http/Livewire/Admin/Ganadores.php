<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Aspirante;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;


class Ganadores extends LivewireDatatable
{
    protected $listeners = [
        'ganador' => 'builder'
    ];
    
    public function builder()
    {
        return Aspirante::where('ganador', '=', '1');
    }
  
    public function columns()
    {
        return [
            Column::name('num_orden')->searchable(),
            Column::raw("CONCAT(apellidos, nombres) AS Titular")->searchable(),
            Column::name('num_doc')->searchable(),
        ];
    }
}
