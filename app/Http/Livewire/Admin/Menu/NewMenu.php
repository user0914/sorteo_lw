<?php

namespace App\Http\Livewire\Admin\Menu;

use App\Models\Menu;
use Livewire\Component;

class NewMenu extends Component
{
    public $menu;
    public $targets;

    protected $rules = [
        'menu.title'    => 'required',
        'menu.position' => 'required|numeric',
        'menu.to_home'  => 'nullable',
        'menu.link'     => 'nullable',
        'menu.icon'     => 'nullable',
        'menu.target'   => 'nullable',
    ];

    public function mount(){
        $this->menu = new Menu();
        $this->menu->target = '_self';
        $this->targets = [
            '_self' => 'En la misma ventana',
            '_blank' => 'En una ventana nueva',
        ];

    }
    
    public function save_menu(){
        $this->validate();

        $this->menu->published = false;
        $this->menu->to_home = $this->menu->to_home ? true : false;
        $this->menu->save();
        
        return redirect()->route('admin.menu.edit', $this->menu->id);
    }

    public function render(){
        return view('livewire.admin.menu.new-menu');
    }
}
