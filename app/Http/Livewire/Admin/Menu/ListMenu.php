<?php

namespace App\Http\Livewire\Admin\Menu;

use App\Models\Menu;
use Livewire\Component;
use Livewire\WithPagination;

class ListMenu extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public function render(){

        $menus = Menu::orderBy('title')
            ->withCount('pages')
            ->paginate();
        return view('livewire.admin.menu.list-menu',[
            'menus' => $menus
        ]);
    }
}
