<?php

namespace App\Http\Livewire\Admin\Menu;

use App\Models\Menu;
use Livewire\Component;

class EditMenu extends Component
{

    public $menu;
    public $targets;
    public $has_pages;

    protected $rules = [
        'menu.title'    => 'required',
        'menu.position' => 'required|numeric',
        'menu.to_home'  => 'nullable',
        'menu.link'     => 'nullable',
        'menu.icon'     => 'nullable',
        'menu.target'   => 'nullable',
    ];

    public function mount(Menu $menu){
        $this->menu = $menu;
        $this->has_pages = $menu->pages()->exists();
        $this->targets = [
            '_self' => 'En la misma ventana',
            '_blank' => 'En una ventana nueva',
        ];
    }
    
    public function save_menu(){
        $this->validate();
        $this->menu->save();

        $this->toast('Menu Actualizado');
    }

    public function toggle_status(){
        $this->menu->update([
            'published' => !$this->menu->published
        ]);
    }

    public function delete_menu(){
        $this->menu->delete();
        return redirect()->route('admin.menu.index');
    }

    public function render(){
        return view('livewire.admin.menu.edit-menu');
    }

    public  function toast($title, $icon = 'success'){
        $this->dispatchBrowserEvent('swal', [
            'title' => $title,
            'timer'=>2000,
            'icon'=> $icon,
            'toast'=>true,
            'position'=>'top-right',
            'showConfirmButton' =>  false,
        ]);
      }
}
