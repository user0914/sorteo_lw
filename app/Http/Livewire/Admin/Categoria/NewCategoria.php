<?php

namespace App\Http\Livewire\Admin\Categoria;

use Livewire\Component;
use App\Models\Categoria;
use App\Http\Traits\toast;

class NewCategoria extends Component
{
    public $categorias;
    use toast;
    
    protected $rules = [
        'categorias.nombre'    => 'required | unique:categorias',
    ];

    public function mount(){
        $this->categorias = new Categoria();
    }
    
    public function save_categoria(){
        $this->validate();

        $this->categorias->save();        

        $this->emit('modalNewCategoria', $this->categorias->id);

        $this->toast('Categoria agregada');
    }

    public function render()
    {
        return view('livewire.admin.categoria.new-categoria');
    }
}
