<?php

namespace App\Http\Livewire\Admin\Categoria;

use Livewire\Component;

class ListCategoria extends Component
{
    public function render()
    {
        return view('livewire.admin.categoria.list-categoria');
    }
}
