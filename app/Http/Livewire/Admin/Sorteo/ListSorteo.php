<?php

namespace App\Http\Livewire\Admin\Sorteo;

use Livewire\Component;
use App\Models\Sorteo;
use Illuminate\Support\Facades\DB;

class ListSorteo extends Component
{

    public function delete(Sorteo $sorteo){
        $sorteo->delete();
        return redirect()->route('admin.sorteos');
    }

    public function soft_delete(Sorteo $sorteo){
        $sorteo->forceDelete();
        return redirect()->route('admin.sorteos');
    }

    public function render()
    {
        $sorteosConAspirantes = Sorteo::groupBy('sorteos.nombre', 'categorias.nombre')
        ->join('aspirantes', 'aspirantes.sorteo_id', 'sorteos.id')
        ->join('categorias', 'aspirantes.categoria_id', 'categorias.id')
        ->select( DB::raw('max(sorteos.id) sorteo_id, sorteos.nombre sorteo, categorias.nombre categoria, count(aspirantes.id) as aspirantes'))
        ->get();

        $idsSorteosConAspirantes = [];

        foreach ($sorteosConAspirantes as $key => $value) {
            $idsSorteosConAspirantes[] = $value->sorteo_id;
        }

        $sorteosConAspirantes = Sorteo::groupBy('sorteos.nombre', 'categorias.nombre')
        ->join('aspirantes', 'aspirantes.sorteo_id', 'sorteos.id')
        ->join('categorias', 'aspirantes.categoria_id', 'categorias.id')
        ->select( DB::raw('max(sorteos.id) sorteo_id, sorteos.nombre sorteo, categorias.nombre categoria, count(aspirantes.id) as aspirantes'))
        // ->get()
        ;

        $todosLosSorteos = Sorteo::groupBy('sorteos.nombre')
        ->select( DB::raw("max(sorteos.id) sorteo_id, sorteos.nombre sorteo, '' categoria, 0 aspirantes "))
        ->whereNotIn('sorteos.id', [$idsSorteosConAspirantes])
        ->orderBy('sorteo')
        ->union($sorteosConAspirantes)
        ->get();


        return view('livewire.admin.sorteo.list-sorteo', [
            'sorteos' => $todosLosSorteos
        ]);
    }
}
