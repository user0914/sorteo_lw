<?php

namespace App\Http\Livewire\Admin\Sorteo;

use App\Models\Sorteo;
use Livewire\Component;
use App\Models\Categoria;

class IndexSorteo extends Component
{

    public $sorteo_selected = 1, $categoria_selected = 1;


    public function mount () {
        $sorteo = Sorteo::orderBy('nombre')->first();
        $categoria = Categoria::orderBy('nombre')->first();
        $this->sorteo_selected = $sorteo->id;
        $this->categoria_selected = $categoria->id;
         
        $this->emit('filtroBuscar', );
    }

    public function buscar () {
        $this->emit('filtroBuscar', [ 'sorteo' => $this->sorteo_selected, 
        'categoria' => $this->categoria_selected ]);
    }

    public function render()
    {        
        $sorteos = Sorteo::orderBy('nombre')->pluck('nombre','id');
        $categorias = Categoria::orderBy('nombre')->pluck('nombre','id');

        return view('admin.sorteo', [
            'sorteos'       => $sorteos,
            'categorias'    => $categorias
        ]);
    }
}
