<?php

namespace App\Http\Livewire\Admin\Sorteo;

use App\Models\Sorteo;
use Livewire\Component;
use App\Http\Traits\toast;

class NewSorteo extends Component
{
    public $sorteos;
    use toast;
    
    protected $rules = [
        'sorteos.nombre'    => 'required | unique:sorteos',
    ];

    public function mount(){
        $this->sorteos = new Sorteo();
    }
    
    public function save_sorteo(){
        $this->validate();

        $this->sorteos->save();        

        $this->emit('modalNewSorteo', $this->sorteos->id);

        $this->toast('Sorteo agregado');
    }

    public function render()
    {
        return view('livewire.admin.sorteo.new-sorteo');
    }
}
