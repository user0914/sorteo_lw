<?php

namespace App\Http\Livewire\Admin\Pages;

use App\Models\Menu;
use App\Models\Page;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class EditPages extends Component
{
    use WithFileUploads;
    public $page;
    public $image;
    public $old_image;

    protected $rules = [
        'page.title'   => 'required',
        'page.content' => 'required',
        'page.menu_id' => 'nullable',
        'image'        => 'image|nullable'
    ];

    public function mount(Page $page){
        $this->page = $page;
        $this->old_image = $page->image;
    }
    
    public function save_page(){
        $this->validate();
        $this->page->menu_id = $this->page->menu_id == '' ? null : $this->page->menu_id;
        $this->page->published = false;
        $this->page->save();
            
        if ($this->image) {
            if($this->old_image) Storage::delete($this->old_image);
            $image_path = $this->image->store('uploads/pages');
            $this->page->update(['image' => $image_path]);
            $this->old_image = $image_path;
            $this->reset('image');
        }

        $this->toast('Pagina Actualizada');
    }

    public function toggle_status(){
        $this->page->update([
            'published' => !$this->page->published
        ]);

        $this->toast(
            $this->page->published ? 'Pagina Publicada' : 'Pagina Despublicada',
            $this->page->published ? 'success' : 'error'
        );
    }

    public function delete_photo(){
        Storage::delete($this->page->image);

        $this->page->update(['image' => null]);
        $this->old_image = null;
    }

    public function delete_page(){
        $this->page->delete();
        if($this->old_image) Storage::delete($this->old_image);
        return redirect()->route('admin.pages.index');
    }
    
    public function render(){

        $menus = Menu::whereNull('link')
            ->where('to_home', 0)
            ->orderBy('title')
            ->pluck('title','id');
        
        return view('livewire.admin.pages.edit-pages', [
            'menus' => $menus
        ]);
    }

    public  function toast($title, $icon = 'success'){
        $this->dispatchBrowserEvent('swal', [
            'title' => $title,
            'timer'=>2000,
            'icon'=> $icon,
            'toast'=>true,
            'position'=>'top-right',
            'showConfirmButton' =>  false,
        ]);
      }
}
