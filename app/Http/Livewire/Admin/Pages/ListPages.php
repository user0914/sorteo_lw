<?php

namespace App\Http\Livewire\Admin\Pages;

use App\Models\Page;
use Livewire\Component;
use Livewire\WithPagination;

class ListPages extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public function render(){

        $pages = Page::orderBy('title')->paginate(8);

        return view('livewire.admin.pages.list-pages', [
            'pages' => $pages
        ]);
    }
}
