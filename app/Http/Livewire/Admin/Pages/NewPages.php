<?php

namespace App\Http\Livewire\Admin\Pages;

use App\Models\Menu;
use App\Models\Page;
use Livewire\Component;
use Livewire\WithFileUploads;

class NewPages extends Component
{
    use WithFileUploads;
    public $page;
    public $image;

    protected $rules = [
        'page.title'   => 'required',
        'page.content' => 'required',
        'page.menu_id' => 'nullable',
        'image'        => 'image|nullable'
    ];

    public function mount(){
        $this->page = new Page();
    }
    
    public function save_page(){
        $this->validate();
        $this->page->menu_id = $this->page->menu_id == '' ? null : $this->page->menu_id;
        $this->page->published = false;
        $this->page->save();
            
        if ($this->image) {
            $image_path = $this->image->store('uploads/pages');
            $this->page->update(['image' => $image_path]);
            $this->reset('image');
        }

        return redirect()->route('admin.pages.edit', $this->page->id);
    }
    
    public function render(){

        $menus = Menu::whereNull('link')
            ->where('to_home', 0)
            ->orderBy('title')
            ->pluck('title','id');
        
        return view('livewire.admin.pages.new-pages', [
            'menus' => $menus
        ]);
    }
}
