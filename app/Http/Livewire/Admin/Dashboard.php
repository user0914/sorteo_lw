<?php

namespace App\Http\Livewire\Admin;

use App\Models\Article;
use App\Models\Page;
use App\Models\Slider;
use App\Models\User;
use Livewire\Component;

class Dashboard extends Component
{

    public $users;
    public $articles;
    public $pages;
    public $sliders;

    public function mount(){
        $this->users    = User::count();
        $this->artilces = Article::count();
        $this->pages    = Page::count();
        $this->sliders  = Slider::count();
    }

    public function render()
    {
        return view('livewire.admin.dashboard');
    }
}
