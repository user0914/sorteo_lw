<?php

namespace App\Http\Livewire\Admin\Slider;

use App\Models\Slider;
use Livewire\Component;
use Livewire\WithPagination;

class ListSlider extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public function render(){

        $sliders = Slider::orderBy('title')->paginate(8);

        return view('livewire.admin.slider.list-slider', [
            'sliders' => $sliders
        ]);
    }
}
