<?php

namespace App\Http\Livewire\Admin\Slider;

use App\Models\Slider;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class EditSlider extends Component
{
    use WithFileUploads;

    public $slider;
    public $image;
    public $old_image;

    protected $rules = [
        'slider.title' => 'required',
        'image'        => 'image'
    ];

    public function mount(Slider $slider){
        $this->slider = $slider;
        $this->old_image = $slider->image;
    }

    public function save_slider(){
        $this->validate();
        $this->slider->published = false;
        $this->slider->save();

        if ($this->image) {
            if ($this->old_image) Storage::delete($this->old_image);
            $image_path = $this->image->store('uploads/sliders');
            $this->slider->update(['image' => $image_path]);
            $this->old_image = $image_path;
            $this->reset('image');
        }

        $this->toast('Carrusel Actualizado');
    }

    public function toggle_status(){
        $this->slider->update([
            'published' => !$this->slider->published
        ]);

        $this->toast(
            $this->slider->published ? 'Carrusel Publicado' : 'Carrusel Despublicado',
            $this->slider->published ? 'success' : 'error'
        );
    }

    public function delete_photo(){
        Storage::delete($this->slider->image);
        $this->slider->update(['image' => null]);
        $this->old_image = null;
    }

    public function delete_slider(){
        $this->slider->delete();
        if($this->old_image) Storage::delete($this->old_image);
        return redirect()->route('admin.slider.index');
    }

    public function render(){
        return view('livewire.admin.slider.edit-slider');
    }

    public  function toast($title, $icon = 'success'){
        $this->dispatchBrowserEvent('swal', [
            'title' => $title,
            'timer'=>2000,
            'icon'=> $icon,
            'toast'=>true,
            'position'=>'top-right',
            'showConfirmButton' =>  false,
        ]);
      }
}
