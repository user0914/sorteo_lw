<?php

namespace App\Http\Livewire\Admin\Slider;

use App\Models\Slider;
use Livewire\Component;
use Livewire\WithFileUploads;

class NewSlider extends Component
{
    use WithFileUploads;
    public $slider;
    public $image;

    protected $rules = [
        'slider.title' => 'required',
        'image'        => 'image'
    ];

    public function mount(){
        $this->slider = new Slider();
    }
    
    public function save_slider(){
        
        $this->validate();
        
        if ($this->image) {
            $image_path = $this->image->store('uploads/sliders');
            $this->slider->image = $image_path;
            $this->slider->published = false;
            $this->slider->save();
            $this->reset('image');
        }

        return redirect()->route('admin.slider.edit', $this->slider->id);
    }

    public function render(){
        return view('livewire.admin.slider.new-slider');
    }
}
