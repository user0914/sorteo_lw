<?php

namespace App\Http\Livewire\Admin\Aspirante;

use Livewire\Component;
use App\Models\Aspirante;
use Livewire\WithPagination;

class ListGanador extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sorteo_selected, $categoria_selected;
    public $categorias, $sorteos;

    public function render()
    {
        $ganadores = Aspirante::orderBy('num_orden')
            ->where('ganador', '1')
            ->when($this->sorteo_selected, function ($query) {
                return $query->where('sorteo_id', $this->sorteo_selected);
            })
            ->when($this->categoria_selected, function ($query) {
                return $query->where('categoria_id', $this->categoria_selected);
            })
            ->paginate(10);

        return view('livewire.admin.aspirante.list-ganador',[
            'aspirantes' => $aspirantes
        ]);
    }
}