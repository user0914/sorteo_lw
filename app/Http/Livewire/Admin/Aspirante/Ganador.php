<?php

namespace App\Http\Livewire\Admin\Aspirante;

use App\Models\Aspirante;
use Livewire\Component;

class Ganador extends Component
{
    public $aspirante;

    public function mount(Aspirante $aspirante) {
        $this->aspirante = $aspirante;
    }
    
    public function render()
    {
        return view('livewire.admin.aspirante.ganador');
    }
}
