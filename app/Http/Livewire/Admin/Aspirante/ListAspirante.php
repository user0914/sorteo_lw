<?php

namespace App\Http\Livewire\Admin\Aspirante;

use App\Models\Sorteo;
use Livewire\Component;
use App\Models\Aspirante;
use App\Models\Categoria;
use App\Http\Traits\toast;
use Livewire\WithPagination;
use Illuminate\Support\Facades\App;

class ListAspirante extends Component
{
    use WithPagination;
    use toast;
    protected $paginationTheme = 'bootstrap';
    public $sorteo_selected = '', $categoria_selected = '', $num_orden, $aspiranteGanador, $data_reporte;
    public $aspiranteGanadorExistente;
    // public $categorias, $sorteos;

    public function edit(Aspirante $aspirante) {
       

        $ganadorAnterior = Aspirante::orderBy('num_orden')
                ->where('ganador', '1')
                ->where('num_doc', $aspirante->num_doc)
                ->first();
//                dd($aspirante->num_doc);

                if($ganadorAnterior) {
                    $this->aspiranteGanadorExistente = $ganadorAnterior;
                } else {
                    $this->aspiranteGanadorExistente = null;
                }

        $this->aspiranteGanador = $aspirante;
//        $this->$aspiranteGanadorExistente = 
            $this->emit('show-form');
        }
        
        public function ganador(Aspirante $aspirante) {
            $ganadorAnterior = Aspirante::find($aspirante->id)
                ->where('num_doc', $aspirante->num_doc)
                ->where('ganador', '1')
                ->first();

            if ($ganadorAnterior) {
                $this->toast('El aspirante ya fue ganador en el sorteo' . $ganadorAnterior->sorteo->nombre . ' - ' . $ganadorAnterior->categoria->nombre, 'error');
                $this->emit('submit-form');
            } else {
            $aspirante->ganador = 1;
            $aspirante->save();
            $this->emit('submit-form');
            $this->toast('Exito', 'success');
        }
    }

    public function reporte($general) {
        $fecha = \Carbon\Carbon::now();
        $ganadores = [];

        if($general == 1) {
            $ganadores = Aspirante::orderBy('num_orden')
                ->where('ganador', '1')
                ->get();
        } else {
            // dd('entro al else');   
            if($this->sorteo_selected != '') {
                $ganadores = Aspirante::orderBy('num_orden')
                ->where('ganador', '1')
                ->when($this->sorteo_selected, function ($query) {
                    return $query->where('sorteo_id', $this->sorteo_selected);
                })
                // ->when($this->categoria_selected, function ($query) {
                //     return $query->where('categoria_id', $this->categoria_selected);
                // })
                ->get();

            } else {
    
                $this->toast('Seleccione un concurso para poder generar el reporte', 'error');
            }
        }

        if($ganadores) {          
            
            $this->data_reporte = [
                'ganadores' => $ganadores,
                'fecha' => $fecha,
                'general' => $general
            ];

            return response()->streamDownload(function () {
                $pdf = \PDF::loadView('reports/ganadores', $this->data_reporte);
                echo $pdf->stream();
            }, 'Listado de ganadores.pdf');
        }

    }
    public function render()
    {
        $sorteos = Sorteo::orderBy('nombre')
            ->pluck('nombre','id');
        

        $categorias = Categoria::orderBy('nombre')
            ->pluck('nombre','id');

        $aspirantes = Aspirante::orderBy('num_orden')
            ->where('ganador', '0')
            ->where('sorteo_id', $this->sorteo_selected)
            ->where('categoria_id', $this->categoria_selected)
             ->when($this->num_orden, function ($query) {
                return $query->where('num_orden', $this->num_orden);
            })
            ->paginate(10);



        $ganadores = Aspirante::orderBy('num_orden')
            ->where('ganador', '1')
            ->where('sorteo_id', $this->sorteo_selected)
            ->where('categoria_id', $this->categoria_selected)

            ->paginate(10);
            

//                        dd($ganadores->total());

        return view('livewire.admin.aspirante.list-aspirante',[
            'aspirantes' => $aspirantes,
            'ganadores' => $ganadores,
            'sorteos' => $sorteos,
            'categorias' => $categorias,
        ]);
    }
}
