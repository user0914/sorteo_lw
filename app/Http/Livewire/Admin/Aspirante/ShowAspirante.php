<?php

namespace App\Http\Livewire\Admin\Aspirante;

use Livewire\Component;
use App\Models\Aspirante;
use App\Http\Traits\toast;

class ShowAspirante extends Component
{
    use toast;
    
    public $aspirante;

    public function mount(Aspirante $id) {
        $this->aspirante = $id;

    }
    public function render()
    {
        return view('livewire.admin.aspirante.show-aspirante');
    }
    public function ganador(){
        //dd($this->aspirante->ganador);
        
        $this->aspirante->update([
            'ganador' => !$this->aspirante->ganador
        ]);

        return redirect()->route('admin.sorteo');

        $this->emit('ganador');

        $this->toast(
            !$this->aspirante->ganador ? 'El aspirante ' . $this->aspirante->nombres . ' ' . $this->aspirante->apellidos . ' fue marcado como ganador' : 'Se quito de la lista',
            !$this->aspirante->ganador ? 'success' : 'error'
        );
    }
}
