<?php

namespace App\Http\Livewire\Admin\News;

use App\Models\Article;
use Livewire\Component;
use Livewire\WithFileUploads;

class NewArticle extends Component
{
    use WithFileUploads;
    public $article;
    public $photo;

    protected $rules = [
        'article.title'   => 'required',
        'article.resumen' => 'required',
        'article.content' => 'required',
        'photo'           => 'image|nullable'
    ];

    public function mount(){
        $this->article = new Article();
        // dd($this->article);
    }
    
    public function save_article(){
        
        $this->validate();
        $this->article->published = false;
        $this->article->save();
            
        if ($this->photo) {
            $photo_path = $this->photo->store('uploads/articles');
            $this->article->update(['photo' => $photo_path]);
            $this->reset('photo');
        }


        return redirect()->route('admin.articles.edit', $this->article->id);

    }

    public function render(){
        return view('livewire.admin.news.new-article');
    }
}
