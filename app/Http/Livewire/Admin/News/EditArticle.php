<?php

namespace App\Http\Livewire\Admin\News;

use App\Models\Article;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class EditArticle extends Component
{
    use WithFileUploads;

    public $article;
    public $photo;
    public $old_photo;

    public function mount(Article $article){
        $this->article = $article;
        $this->old_photo = $article->photo;
    }

    protected $rules = [
        'article.title'   => 'required',
        'article.resumen' => 'required',
        'article.content' => 'required',
        'photo'           => 'image|nullable'
    ];

    public function save_article(){
        $this->validate();
        $this->article->save();
            
        if ($this->photo) {
            if($this->old_photo) Storage::delete($this->old_photo);
            $photo_path = $this->photo->store('uploads/articles');
            $this->article->update(['photo' => $photo_path]);
            $this->old_photo = $photo_path;
            $this->reset('photo');
        }

        $this->toast('Noticia Actualizada');
    }

    public function toggle_status(){
        $this->article->update([
            'published' => !$this->article->published
        ]);

        $this->toast(
            $this->article->published ? 'Noticia Publicada' : 'Noticia Despublicada',
            $this->article->published ? 'success' : 'error'
        );
    }

    public function delete_article(){
        $this->article->delete();
        if($this->old_photo) Storage::delete($this->old_photo);
        return redirect()->route('admin.articles.index');
    }

    
    public function delete_photo(){
        Storage::delete($this->article->photo);
        $this->article->update(['photo' => null]);
        $this->old_photo = null;
    }
    
    public function render(){
        return view('livewire.admin.news.edit-article');
    }

    public  function toast($title, $icon = 'success'){
        $this->dispatchBrowserEvent('swal', [
            'title' => $title,
            'timer'=>2000,
            'icon'=> $icon,
            'toast'=>true,
            'position'=>'top-right',
            'showConfirmButton' =>  false,
        ]);
      }
}
