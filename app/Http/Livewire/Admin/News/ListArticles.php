<?php

namespace App\Http\Livewire\Admin\News;

use App\Models\Article;
use Livewire\Component;
use Livewire\WithPagination;

class ListArticles extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public function render(){

        $articles = Article::orderByDesc('created_at')
            ->paginate(4);

        return view('livewire.admin.news.list-articles',[
            'articles' => $articles
        ]);
    }
}
