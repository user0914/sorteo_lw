<?php

namespace App\Http\Livewire\Admin;

use App\Models\Sorteo;
use Livewire\Component;
use App\Models\Categoria;
use App\Http\Traits\toast;
use Livewire\WithFileUploads;
use App\Imports\AspiranteImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportGrilla extends Component
{
    use toast;
    use WithFileUploads;
    protected $listeners = [
        'modalNewCategoria' => 'categoria_selected',
        'modalNewSorteo' => 'sorteo_selected'
    ];

    public $iteration = 0, $file;
    public $sorteo_selected = 1;
    public $categoria_selected = 1;

    public function mount(){
        $this->user = Auth()->user();

    }

    public function categoria_selected($id) {
        $this->categoria_selected = $id;
    }

    public function sorteo_selected($id) {
        $this->sorteo_selected = $id;
    }


    public function save_grilla() {
        Excel::import(new AspiranteImport($this->categoria_selected, $this->sorteo_selected), $this->file);

//        Excel::import(new AspiranteImport($this->categoria_selected, $this->sorteo_selected), $this->file);

        $this->iteration++;
        $this->reset('file');
        $this->toast('La grilla se cargo exitosamente');
    }

    public function render(){
        $categorias = Categoria::orderBy('nombre')->pluck('nombre','id');
        $sorteos = Sorteo::orderBy('nombre')->where('activo', 1)->pluck('nombre','id');

        return view('livewire.admin.import-grilla', [
            'categorias' => $categorias,
            'sorteos' => $sorteos
        ]);
    }

}
