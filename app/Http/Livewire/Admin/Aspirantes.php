<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Aspirante;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;


class Aspirantes extends LivewireDatatable
{
    protected $listeners = [
        'ganador' => 'builder'
    ];
    
    public function builder()
    {
        return Aspirante::where('ganador', '=', '0');
    }
  
    public function columns()
    {
        return [
            Column::name('id')->hide(),
            Column::name('sorteo_id')->searchable()->hide(),
            Column::name('categoria_id')->searchable()->hide(),
            Column::name('sorteo.nombre')->label('Sorteo')->filterable(),
            Column::name('categoria.nombre')->label('Categoria')->filterable(),

            Column::name('num_orden')->searchable(),
            Column::raw("CONCAT(apellidos, nombres) AS Titular")->searchable(),
            Column::name('num_doc')->searchable(),
            Column::name('ganador')->view('datatableview.modalganador'),        
        ];
    }

}
