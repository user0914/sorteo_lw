<?php

namespace App\Http\Livewire\Admin\Profile;

use Livewire\Component;

class Account extends Component
{

    public $user;
    
    protected $rules = [
        'user.name'      => 'required|string|max:255',
        'user.last_name' => 'required|string|max:255',
        'user.email'     => 'nullable',
    ];
    
    public function mount(){
        $this->user = auth()->user();
    }

    public function save_profile(){
        $this->validate();
        $this->user->save();
        $this->emit('profile_update');

        $this->toast('Perfil Actualizado');
    }

    public function render(){
        return view('livewire.admin.profile.account');
    }

    public  function toast($title, $icon = 'success'){
        $this->dispatchBrowserEvent('swal', [
            'title' => $title,
            'timer'=>2000,
            'icon'=> $icon,
            'toast'=>true,
            'position'=>'top-right',
            'showConfirmButton' =>  false,
        ]);
      }
}
