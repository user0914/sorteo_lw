<?php

namespace App\Http\Livewire\Admin\Profile;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Password extends Component
{
	public $user;
	public $old_password;
	public $new_password;
	public $new_password_confirmation;

	protected $rules = [
		'old_password'              => 'required',
		'new_password'              => 'required|min:6|confirmed',
		'new_password_confirmation' => 'required'
	];

	public function mount(){
		$this->user = User::find(Auth::id());
	}

	public function update_password(){
		$this->validate();

		if (Hash::check($this->old_password, Auth::user()->password)) {
			$this->user->update([
				'password' => Hash::make($this->new_password)
			]);

			$this->toast('Contraseña Actualizada');

			$this->reset('old_password', 'new_password', 'new_password_confirmation');
		} else {
			$this->addError('old_password', 'La Contraseña Actual es incorrecta');
		}
	}

	public function render(){
		return view('livewire.admin.profile.password');
	}

	public  function toast($title, $icon = 'success'){
		$this->dispatchBrowserEvent('swal', [
			'title' => $title,
			'timer' => 2000,
			'icon' => $icon,
			'toast' => true,
			'position' => 'top-right',
			'showConfirmButton' =>  false,
		]);
	}
}
