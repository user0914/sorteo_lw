<?php

namespace App\Http\Livewire\Admin\Profile;

use Livewire\Component;

class NavbarProfile extends Component
{

    protected $listeners = ['profile_update'];


    public function profile_update(){}

    public function render(){
        return view('livewire.admin.profile.navbar-profile',[
            'name' => auth()->user()->name.' '.auth()->user()->last_name
        ]);
    }
}
