<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Hash;
use Livewire\Component;

class NewUsers extends Component {
    public $user;
    public $password_confirmation;
    public $password;
    public $edit = false;

    protected $rules = [
        'user.name'             => 'required|string|max:255',
        'user.last_name'        => 'required|string|max:255',
        'user.email'            => 'required|string|email|max:255|unique:users,email',
        'password'              => 'required|min:8|confirmed',
        'password_confirmation' => 'required'
    ];

    public function mount(){
        $this->user = new User();
    }
    
    public function save_user(){
        $this->validate();
        $this->user->active   = false;
        $this->user->password = Hash::make($this->password);
        $this->user->save();
        
        return redirect()->route('admin.users.edit', $this->user->id);
    }

    public function render(){
        return view('livewire.admin.users.new-users');
    }
}
