<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Livewire\Component;

class EditUsers extends Component {


    public $user;
    public $edit = true;

    protected $rules = [
        'user.name'      => 'required|string|max:255',
        'user.last_name' => 'required|string|max:255',
        'user.email'     => '',
    ];

    public function mount(User $user){
        $this->user = $user;
    }
    
    public function save_user(){
        if($this->user->id == auth()->id()){
            $this->toast('No se puede actualizar uno mismo','error');
        }else{
            $this->validate();
            $this->user->save();
            
            $this->toast('Usuario Actualizado');
        }
    }
    
    public function toggle_status(){
        
        if($this->user->id == auth()->id()){
            $this->toast('No se puede cambiar el estado de uno mismo','error');
        }else{
            $this->user->update([
                'active' => !$this->user->active
            ]);

            $this->toast(
                $this->user->published ? 'User Activo' : 'User Suspendido',
                $this->user->published ? 'success' : 'error'
            );
        }
    }

    public function delete_user(){
        $this->user->delete();
        return redirect()->route('admin.users.index');
    }

    public function render()
    {
        return view('livewire.admin.users.edit-users');
    }

    public  function toast($title, $icon = 'success'){
        $this->dispatchBrowserEvent('swal', [
            'title' => $title,
            'timer'=>2000,
            'icon'=> $icon,
            'toast'=>true,
            'position'=>'top-right',
            'showConfirmButton' =>  false,
        ]);
      }
}
