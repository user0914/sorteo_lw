<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class ListUsers extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public function render(){
        $users = User::orderBy('last_name')
            ->orderBy('name')
            ->paginate(8);

        return view('livewire.admin.users.list-users',[
            'users' => $users
        ]);
    }
}
