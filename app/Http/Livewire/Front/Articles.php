<?php

namespace App\Http\Livewire\Front;

use App\Models\Article;
use Livewire\Component;
use Livewire\WithPagination;

class Articles extends Component{

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public function render(){

        $articles = Article::where('published',true)
            ->orderByDesc('created_at')
            ->paginate();

        return view('livewire.front.articles', [
            'articles' => $articles
        ]);
    }
}
