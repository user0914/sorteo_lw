<?php

namespace App\Http\Livewire\Front\Home;

use App\Models\Slider;
use Livewire\Component;

class SliderHome extends Component
{
    public function render(){

        return view('livewire.front.home.slider-home',[
            'sliders' => Slider::wherePublished(true)->get()
        ]);
    }
}
