<?php

namespace App\Http\Livewire\Front\Home;

use App\Models\Article;
use Livewire\Component;

class ArticlesHome extends Component
{
    public function render(){
        $articles = Article::orderByDesc('created_at')
            ->where('published', true)
            ->take(4)
            ->get();

        return view('livewire.front.home.articles-home',[
            'articles' => $articles
        ]);
    }
}
