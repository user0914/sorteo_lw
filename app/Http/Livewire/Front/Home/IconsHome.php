<?php

namespace App\Http\Livewire\Front\Home;

use App\Models\Menu;
use Livewire\Component;

class IconsHome extends Component
{
    public function render(){

        $icons = Menu::where('to_home', true)
            ->orderBy('position')
            ->take(6)
            ->get();

        return view('livewire.front.home.icons-home',[
            'icons' => $icons
        ]);
    }
}
