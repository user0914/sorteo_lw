<?php

namespace App\Http\Livewire\Front;

use App\Models\Article;
use Livewire\Component;

class ArticlesShow extends Component
{
    public $article;
    public $more_articles;

    public function mount(Article $article){
        $this->article = $article;
        $this->more_articles = Article::where('id', '<>', $article->id)
            ->where('published', true)
            ->orderByDesc('created_at')
            ->take(5)
            ->get();
    }

    public function render(){
        return view('livewire.front.articles-show');
    }
}
