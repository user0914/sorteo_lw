<?php

namespace App\Http\Livewire\Front\Menu;

use App\Models\Menu;
use Livewire\Component;

class DynamicMenu extends Component
{
    public function render(){

        $menus = Menu::whereToHome(false)
            ->with('pages')
            ->orderBy('position')
            ->get();

        return view('livewire.front.menu.dynamic-menu', [
            'menus' => $menus
        ]);
    }
}
