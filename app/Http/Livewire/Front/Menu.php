<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class Menu extends Component
{
    public function render()
    {
        return view('livewire.front.menu');
    }
}
