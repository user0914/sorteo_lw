<?php

namespace App\Http\Livewire\Front;

use App\Models\Page;
use Livewire\Component;

class PageShow extends Component
{
    public $page;

    public function mount(Page $page){
        $this->page = $page;
    }

    public function render()
    {
        return view('livewire.front.page-show');
    }
}
