<?php

namespace App\Imports;


use App\Models\Aspirante;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;

class AspiranteImport implements ToModel, WithStartRow
{
    public $_categoria_id = null;
    public $_sorteo_id = null;

    public function __construct($categoria_id, $sorteo_id) {
        $this->_categoria_id = $categoria_id;
        $this->_sorteo_id = $sorteo_id;
    }

    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        $length = 8;
        $string = substr(str_repeat(0, $length).$row[4], - $length);
        $num_doc = substr_replace($string, '.', 2, 0);
        $num_doc = substr_replace($num_doc, '.', 6, 0);
 
        $num_orden = str_pad($row[0], 4, '0', STR_PAD_LEFT);
        return new Aspirante([
            'num_orden'         => $num_orden,
            'num_inscripcion'   => $row[1],
            'nombres'           => $row[2],
            'apellidos'         => $row[3],
            'num_doc'           => $num_doc,
            'categoria_id'      => $this->_categoria_id,
            'sorteo_id'         => $this->_sorteo_id,
            'ganador'           => '0',
        ]);

    }
    
}
